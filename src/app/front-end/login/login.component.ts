import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  error_msg: any;
  isLoading: boolean;

  constructor(private api: ApiService, private router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
      this.loginForm = this.fb.group({
        username: ['', [Validators.required]],
        password: ['', [Validators.required]]
      });
      
  }

  logIn(){
    if (this.loginForm.valid) {
      this.onSubmitlogin(this.loginForm.value);
     
    }

   
  }
  onSubmitlogin(data){
   
    let response = this.api.login(data);
    response.subscribe((response: any)=>{
     
      if (response) {
        this.api.setLoadingStatus(false);
          window.sessionStorage.setItem("profile", JSON.stringify(response));
          this.router.navigate(["/dashboard"]);
        }
        else {
         
        this.router.navigate(["/login"]);
        }
        
      
    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Payment required to use the system"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong Username or Password"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })

  }
}
