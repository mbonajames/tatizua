import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontEndRoutingModule } from './front-end-routing.module';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { IndexComponent } from './index/index.component';
import { TripDetailsComponent } from './trip-details/trip-details.component';
import { SampleItinerariesComponent } from './sample-itineraries/sample-itineraries.component';
import { TripsComponent } from './trips/trips.component';
import { PricingComponent } from './pricing/pricing.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { ApiService } from './api.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AccommodationDetailsComponent } from './accommodation-details/accommodation-details.component';
import { DestinationDetailsComponent } from './destination-details/destination-details.component';


@NgModule({
  declarations: [NavBarComponent, IndexComponent, TripDetailsComponent, SampleItinerariesComponent, TripsComponent, PricingComponent, AboutUsComponent, LoginComponent, SignupComponent, AccommodationDetailsComponent, DestinationDetailsComponent],
  imports: [
    CommonModule,
    FrontEndRoutingModule,
    DashboardModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [ApiService],
})
export class FrontEndModule { }
