import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { IndexComponent } from './index/index.component';
import { TripDetailsComponent } from './trip-details/trip-details.component';
import { SampleItinerariesComponent } from './sample-itineraries/sample-itineraries.component';
import { TripsComponent } from './trips/trips.component';
import { PricingComponent } from './pricing/pricing.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AccommodationDetailsComponent } from './accommodation-details/accommodation-details.component';
import { DestinationDetailsComponent } from './destination-details/destination-details.component';

const routes: Routes = [
  {path: '', component: NavBarComponent, children: [
    {path: '', component: IndexComponent},
    {path: 'trip-details', component: TripDetailsComponent},
    {path: 'trips', component: TripsComponent},
    {path: 'pricing', component: PricingComponent},
    {path: 'about-us', component: AboutUsComponent},
    {path: 'login', component: LoginComponent},
    {path: 'signup', component: SignupComponent},
    {path: 'accommodation-details/:id', component: AccommodationDetailsComponent},
    {path: 'destination-details/:id', component: DestinationDetailsComponent}

    // {path: 'pending-deleted-services', component: PendingDeletedServicesComponent},
    // {path: 'feedback', component: FeedbackComponent}
  ]},
  {path: 'sample-itinerary', component: SampleItinerariesComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontEndRoutingModule { }
