import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  isLoading: boolean;

  constructor(private api: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
      this.signupForm = this.fb.group({
        name: ['', [Validators.required]],
        email: ['', [Validators.required]],
        phone: ['', [Validators.required]],
        logo: ['', [Validators.required]],
        website: ['', [Validators.required]],
        username: ['', [Validators.required]],
        password: ['', [Validators.required, Validators.minLength(8)]]
      });
  }

  signUp(){
    if (this.signupForm.valid) {
      this.signupForm.value.permission = 'allowed';
      this.onSubmitSignup(this.signupForm.value);
     
    }   
  }

  onSubmitSignup(data) {
    let response = this.api.signup(data);
    response.subscribe((response: any)=>{
      this.signupForm = response;
      console.log('Signup===', this.signupForm);
    },(error)=> {
      this.api.setLoadingStatus(false);
    });
  }

}
