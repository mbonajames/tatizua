import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-destination-details',
  templateUrl: './destination-details.component.html',
  styleUrls: ['./destination-details.component.scss']
})
export class DestinationDetailsComponent implements OnInit {
  id: any;
  isLoading: boolean;
  destination: any;

  constructor(private api:ApiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });

      this.getDestination(this.id)
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
  }

  getDestination(id){
    this.api.getDestination(id).subscribe((response)=>{
      this.destination = response;
      console.log(this.destination);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
}
