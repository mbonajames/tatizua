import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public error: BehaviorSubject<string> = new BehaviorSubject<string>('');

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  TOKEN: String;

 //BASE_URL = 'http://localhost:8000/api/';
 BASE_URL = 'https://tuasoft-api.herokuapp.com/api/';

  constructor(private http: HttpClient) {}

  // LOADING STATUS FOR INDICATOR
  setLoadingStatus(status: boolean) {
    this.isLoading.next(status);
  }

  login(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'login';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  }

  signup(data) {
    this.setLoadingStatus(true);

    const url = this.BASE_URL + 'signup';

    const response = this.http.post(url, data, this.httpOptions);
    
    return response;
  }

  getItinerary(id) {
    this.setLoadingStatus(true);

     const url = this.BASE_URL + 'get-itinerary?id=42160fb6-0406-48a1-8ded-6915e3aa1cb7';
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }

  getAccomodation(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-accomodation?id=' + id;
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
 
  getDestination(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-destination?id=' + id;
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
  
}

