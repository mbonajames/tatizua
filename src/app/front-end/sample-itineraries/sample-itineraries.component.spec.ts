import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SampleItinerariesComponent } from './sample-itineraries.component';

describe('SampleItinerariesComponent', () => {
  let component: SampleItinerariesComponent;
  let fixture: ComponentFixture<SampleItinerariesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleItinerariesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleItinerariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
