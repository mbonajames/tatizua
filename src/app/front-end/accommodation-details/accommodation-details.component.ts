import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-accommodation-details',
  templateUrl: './accommodation-details.component.html',
  styleUrls: ['./accommodation-details.component.scss']
})
export class AccommodationDetailsComponent implements OnInit {
  id: any;
  isLoading: boolean;
  accomodation: any;

  constructor(private api:ApiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });

      this.getAccomodation(this.id)
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
  }

  getAccomodation(id){
    this.api.getAccomodation(id).subscribe((response)=>{
      this.accomodation = response;
      console.log(this.accomodation);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
}
