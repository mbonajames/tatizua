import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SampleItinerariesComponent } from './sample-itineraries/sample-itineraries.component';
import { ItineraryComponent } from './itinerary/itinerary.component';

const routes: Routes = [
  {path: 'itinerary', component: SampleItinerariesComponent},
  {path: 'daily-information', component: ItineraryComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MobileRoutingModule { }
