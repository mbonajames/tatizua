import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MobileRoutingModule } from './mobile-routing.module';
import { SampleItinerariesComponent } from './sample-itineraries/sample-itineraries.component';
import { ItineraryComponent } from './itinerary/itinerary.component';


@NgModule({
  declarations: [SampleItinerariesComponent, ItineraryComponent],
  imports: [
    CommonModule,
    MobileRoutingModule
  ]
})
export class MobileModule { }
