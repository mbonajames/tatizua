import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../front-end/api.service';

@Component({
  selector: 'app-sample-itineraries',
  templateUrl: './sample-itineraries.component.html',
  styleUrls: ['./sample-itineraries.component.scss']
})
export class SampleItinerariesComponent implements OnInit {
  id: any;
  isLoading: boolean;
  itinerary: any;

  constructor(private api:ApiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });

      this.getItinerary(this.id)
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
  }

  getItinerary(id){
    this.api.getItinerary(id).subscribe((response)=>{
      this.itinerary = response;
      console.log(this.itinerary);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
}
