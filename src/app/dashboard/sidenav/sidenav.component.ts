import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  
  isAuthenticated:boolean;
  company:any;
  constructor(private api:ApiService,private router:Router) { }

  ngOnInit(): void {
    
    const profile= JSON.parse(window.sessionStorage.getItem("profile"));
    


    if (profile) {
      this.isAuthenticated = true 
    }
    if (!profile) {
     
      this.router.navigate(["/login"]);
    
  } 
 

  }


  logOut(){
    window.sessionStorage.clear();
    this.router.navigate(["/login"]);
  }

}
