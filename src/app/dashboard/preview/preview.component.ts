import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  id: any;
  profile: any;
  isLoading: boolean;
  itinerary: any;
  error_msg: any;
  company: any;
  country: any;
  
  constructor(private api: ApiService, private router: Router, private route: ActivatedRoute ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      this.getItinerary(this.id)
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
  }

  getItinerary(id){
    this.api.getItinerary(id).subscribe((response)=>{
      this.itinerary = response;
      this.addCosts(this.itinerary);
      this.api.setLoadingStatus(false);
      console.log(this.itinerary)
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
addCosts(itinerary){
  var data = 
            {
              cost: itinerary.it_cost,
              profit: itinerary.markup_cost,
              total_cost: itinerary.total, 
            };
            console.log(data)
  this.api.updateCosts(itinerary.id,data).subscribe((response)=>{
    this.api.setLoadingStatus(false);
  },error =>{
    this.api.setLoadingStatus(false);
  })
}

}
