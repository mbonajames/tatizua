import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  itineraries:any;
  id:any;
  periodForm:FormGroup;
  profile:any;
  reports:any;
  start:any;
  end:any;


  constructor(private api: ApiService,private fb: FormBuilder,private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const company_id = this.profile.id
    this.periodForm = this.fb.group({
      company: [company_id, [Validators.required]],
      startDate: [' ', [Validators.required]],
      endDate: [' ', [Validators.required]]
    });
 
    
  }


  getItinireraries(id){
    this.api.getItineraries(id).subscribe((response)=>{
      this.itineraries = response;
    })
  }

  searchReport(data){
    this.start = data.startDate
    this.end = data.endDate
    let response = this.api.getReport(data);
    
    response.subscribe((data:any)=>{
      this.reports = data;
      console.log(this.reports)
      
      //this.api.setLoadingStatus(false);
      //let price = 0
      //for(let i=0;i<this.reports.data.length;i++){
       // let data = this.reports.data[i];
      //}

    })
  

  }
  

}
