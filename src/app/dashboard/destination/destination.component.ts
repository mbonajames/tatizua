import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.scss']
})
export class DestinationComponent implements OnInit {
  destinationsForm: FormGroup;
  id: any;
  isLoading: boolean;
  error_msg: any;
  dailyDestination: any;
  services: any;
  destinations: any;
  itinerary: any;
  dailydestinations: any;
  passed_destination: any;
  passed_itinerary: any;
  constructor(private fb: FormBuilder, private api: ApiService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.passed_destination= JSON.parse(window.sessionStorage.getItem("destination"));
    this.passed_itinerary= JSON.parse(window.sessionStorage.getItem("passitinerary"));
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      this.itinerary= JSON.parse(window.localStorage.getItem("itinerary"));
      
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
      this.getServices()
      this.getDestinations()
      this.getDailyDestinations(this.id)
   
    this.destinationsForm = this.fb.group({
      day: [this.id, [Validators.required]],
      service: [this.passed_destination ? this.passed_destination.service:'', [Validators.required]],
      name: [this.passed_destination ? this.passed_destination.name:'', [Validators.required]],
      location: [this.passed_destination ? this.passed_destination.location:'', [Validators.required]],
      distance: [this.passed_destination ? this.passed_destination.distance:'', [Validators.required]],
      duration: [this.passed_destination ? this.passed_destination.duration:'', [Validators.required]],
      pickup: [this.passed_destination ? this.passed_destination.pickup:'', [Validators.required]],
    });
  }

  addDestination(data) {
    let response = this.api.addDailyDestination(data);
    response.subscribe((response: any)=>{
     
      if (response) {
        this.api.setLoadingStatus(false);
        this.dailyDestination = response
        this.ngOnInit();
        
        }
        else {
          this.api.setLoadingStatus(false);
      
        }
        
      
    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Payment required to use the system"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong Username or Password"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })

  }
  getServices(){
    this.api.getServices().subscribe((response)=>{
      this.services = response;
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  getDestinations(){
    this.api.getDestinations().subscribe((response)=>{
      this.destinations = response;
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  getDailyDestinations(id){
    this.api.getDailydestinations(id).subscribe((response)=>{
      this.dailydestinations = response;
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  next(){
    this.router.navigate(['/dashboard/day', this.itinerary]);
  }
  preview(){
    this.router.navigate(['/dashboard/preview', this.itinerary]);
  }
  save(id){
    this.api.updateDestination(id,this.destinationsForm.value).subscribe((response)=>{
      console.log(this.destinationsForm.value)
      this.api.setLoadingStatus(false);
      this.router.navigate(['/dashboard/edit-itinerary', this.passed_itinerary]);
      window.sessionStorage.removeItem("destination");
      window.sessionStorage.removeItem("passitinerary");
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  newSave(){
    this.router.navigate(['/dashboard/edit-itinerary', this.passed_itinerary]);
    window.sessionStorage.removeItem("passitinerary");
  }
}
