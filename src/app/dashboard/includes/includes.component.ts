import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-includes',
  templateUrl: './includes.component.html',
  styleUrls: ['./includes.component.scss']
})
export class IncludesComponent implements OnInit {
  includesForm: FormGroup;
  id: any;
  profile: any;
  isLoading: boolean;
  services: any;
  error_msg: any;
  includes: any;
  passed_itinerary: any;
  constructor(private fb: FormBuilder, private api: ApiService, private router: Router, private route: ActivatedRoute ) {}

  ngOnInit(): void {
    this.passed_itinerary= JSON.parse(window.sessionStorage.getItem("passitinerary"));
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
      this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
      const company_id = this.profile.id

      this.getServices()
      this.getIncludes(this.id)
   
    this.includesForm = this.fb.group({
      itinerary: [this.id, [Validators.required]],
      service: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
    
  }

  addInclude(data){
   
    let response = this.api.addInclude(data);
    response.subscribe((response: any)=>{
     
      if (response) {
        this.api.setLoadingStatus(false);
        this.ngOnInit();
        }
        else {
          this.api.setLoadingStatus(false);
      
        }
        
      
    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Payment required to use the system"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong Username or Password"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })
  }
  getServices(){
    this.api.getServices().subscribe((response)=>{
      this.services = response;
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  getIncludes(id){
    this.api.getIncludes(id).subscribe((response)=>{
      this.includes = response;
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  next(){
    this.router.navigate(['/dashboard/excludes', this.id]);
  }
  newSave(){
    this.router.navigate(['/dashboard/edit-itinerary', this.passed_itinerary]);
    window.sessionStorage.removeItem("passitinerary");
  }
}
