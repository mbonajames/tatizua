import { Component, OnInit } from '@angular/core';
import {Validators,FormBuilder,FormControl,FormGroup} from '@angular/forms';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit 
{
  contacts:any;
  id:any;
  isLoading:boolean;
  addServiceForm: FormGroup;
  profileData:any;
  edit:boolean;
  company:any;
  profileForm :any;
  message:any;
  profile:any;


  
       //Image upload
       // Variable to store shortLink from api response 
    shortLink: string = ""; 
    loading: boolean = false; // Flag variable 
    file: File = null; // Variable to store file 


  constructor(private fb: FormBuilder,private api: ApiService,private router: Router, private route: ActivatedRoute ) { }


  ngOnInit(): void {
    const profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const company_id = profile.id
    this.getcontact(company_id);
    this.getCompany(company_id,profile)
   
    
    this.api.isLoading.subscribe((status: boolean) => {
    this.isLoading = status;
    });
    
    this.profileForm = this.fb.group({
      company: [company_id, [Validators.required]],
      name:[profile?profile.name:'', [Validators.required]],
      email:[profile?profile.email:'', [Validators.required]],
      phone:[profile?profile.phone:'', [Validators.required]],
      website:[profile?profile.website:'', [Validators.required]],
    });
    
   
  }

  getcontact(id){
    this.api.getContact(id).subscribe((response)=>{
      this.contacts = response;
     console.log(this.contacts)
    },error =>{
      this.api.setLoadingStatus(false);
    })
  }

  getCompany(id,data){
    this.api.getCompany(id,data).subscribe((response)=>{
      this.company = response;
      console.log(this.company)
    })

  }
 
      // On file Select 
  onChange(event) { 
    this.file = event.target.files[0]; 
    }

    // OnClick of button Upload listener
    onUpload() 
    { 
      this.loading = !this.loading; 
      
      this.api.upload(this.file).subscribe((event: any) => { 
              if (typeof (event) === 'object') { 

                  // Short link via api response 
                  this.shortLink = event.link; 

                  this.loading = false; // Flag variable  
              } 
          } 
      ); 
      console.log(this.shortLink); 
  } 

 

//editing
openEditor(company){
  this.profile = company
  this.edit= true
  this.ngOnInit()
  
 }
 editCompany(){
  let response = this.api.updateCompany(this.company.id,this.profileForm.value);
  response.subscribe((res:any)=>{
    this.api.setLoadingStatus(false);
      this.message = "company is successfully updated";
      this.profileData= null
      this.edit= false
      this.ngOnInit()
  },  error =>{
    this.api.setLoadingStatus(false);
    if (error.status == 400) {
      this.message = "service is not updated, Wrong Inputs";
    }
    else if (error.status == 0) {
      this.message = "Not updated, No internet Available";
    }
    
  })

 }
 editContact(data:any,id){
   
  window.sessionStorage.setItem("contact", JSON.stringify(data));
  window.sessionStorage.setItem("passContact", JSON.stringify(this.id));
  this.router.navigate(['/dashboard/edit-contact', id]);
 }


}
