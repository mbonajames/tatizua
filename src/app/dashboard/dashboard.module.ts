import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddItineraryComponent } from './add-itinerary/add-itinerary.component';
import { ItinerariesComponent } from './itineraries/itineraries.component';
import { ProfileComponent } from './profile/profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from './api.service';
import { HttpClientModule } from '@angular/common/http';
import { DayComponent } from './day/day.component';
import { AccomodationComponent } from './accomodation/accomodation.component';
import { DestinationComponent } from './destination/destination.component';
import { IncludesComponent } from './includes/includes.component';
import { ExcludesComponent } from './excludes/excludes.component';
import { PreviewComponent } from './preview/preview.component';
import { EditItineraryComponent } from './edit-itinerary/edit-itinerary.component';
import { ReportsComponent } from './reports/reports.component';
import { AddEditContactComponent } from './add-edit-contact/add-edit-contact.component';
import { ServiceComponent } from './service/service.component';



@NgModule({
  declarations: [SidenavComponent, DashboardComponent, AddItineraryComponent, ItinerariesComponent, ProfileComponent, DayComponent, AccomodationComponent, DestinationComponent, IncludesComponent, ExcludesComponent, PreviewComponent, EditItineraryComponent, ReportsComponent, AddEditContactComponent, ServiceComponent,],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [ApiService],
  exports: [DashboardComponent],
})
export class DashboardModule { }
