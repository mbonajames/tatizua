import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {
  id:any;
  services:any;
  isAddMode:boolean;
  edit:boolean;
  isadded:boolean;
  addServiceForm: FormGroup;
  editServiceForm: FormGroup;
  message: any;
  errormessage:any;
  isLoading: boolean;
  error:any;
  date=new Date();
  ServiceData: any;
  delete_id: any;
  delete_name:any;
  delete_service: any;
  serviceData:any;
  company: any;
  profile:any;
  



  constructor(private api:ApiService,private formBuilder: FormBuilder,private router: Router, private route: ActivatedRoute ) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
      const company_id = this.profile.id
        this.getService(company_id);
        this.getALlServices();

    this.addServiceForm = this.formBuilder.group({

      company: [company_id, [Validators.required]],
      service: [this.serviceData ? this.serviceData .service :'', [Validators.required]],
      phone: [this.serviceData ? this.serviceData .phone :'', [Validators.required]],
      price: [this.serviceData ? this.serviceData .price :'', [Validators.required]],
    });
   
        
  }


  getALlServices(){
    let response= this.api.getServices();
    response.subscribe((data)=>{
      this.api.setLoadingStatus(false);
      this.services =data;
    } , error =>{
      
        if (error.status == 403) {
         this.api.setLoadingStatus(false);
          this.errormessage= "services are unavailable"
        }
        else if (error.status == 400) {
         this.api.setLoadingStatus(false);
         this.errormessage = "service is not found"
        }
        else if (error.status == 0) {
         this.api.setLoadingStatus(false);
         this.errormessage = "check your network and try again later"
        }
      
    })
  }


getService(id){
  this.api.getService(id).subscribe((data)=>{
    this.services =data;
    
  })
}


addService(data){
  let response = this.api.addService(data);
  response.subscribe((response: any)=>{
   
    if (response) {
      this.ngOnInit();
      this.api.setLoadingStatus(false);
      this.message='service is added successfully '
      window.localStorage.setItem("service", JSON.stringify(response.id));
        this.router.navigate(['/dashboard/',response.id]);
        
        
      } }, error =>{
      
        if (error.status == 403) {
         this.api.setLoadingStatus(false);
          this.isadded = true;
          this.message = "is not added try again later"
        }
        else if (error.status == 400) {
         this.api.setLoadingStatus(false);
          this.isadded = true;
          this.message = "invalid inputs"
        }
        else if (error.status == 0) {
         this.api.setLoadingStatus(false);
          this.isadded = true;
          this.message = "check your network and try agin later"
        }
        
        
      })


}

//editing
  openEditor(service){
    this.serviceData = service
    this.edit= true
    this.message = null
    this.ngOnInit()
    
   }
 
  editService(){
      let response = this.api.updateService(this.serviceData.id,this.addServiceForm.value);
      response.subscribe((res:any)=>{
        this.api.setLoadingStatus(false);
          this.message = "service is successfully updated";
          this.serviceData = null
          this.edit= false
          this.ngOnInit()
      },  error =>{
        this.api.setLoadingStatus(false);
        if (error.status == 400) {
          this.message = "service is not updated, Wrong Inputs";
        }
        else if (error.status == 0) {
          this.message = "Not updated, No internet Available";
        }
        
      })
  


}

openDelete(id,name){
  this.delete_id = id
  this.delete_service = name
 }

 delete(type, id){
  this.api.delete(type,id).subscribe((response)=>{
  
    console.log(response)
    this.ngOnInit()
  },error =>{
    this.api.setLoadingStatus(false);
  })
}


}