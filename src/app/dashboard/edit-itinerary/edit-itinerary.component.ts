import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-edit-itinerary',
  templateUrl: './edit-itinerary.component.html',
  styleUrls: ['./edit-itinerary.component.scss']
})
export class EditItineraryComponent implements OnInit {

  id: any;
  profile: any;
  isLoading: boolean;
  itinerary: any;
  error_msg: any;
  company: any;
  country: any;
  
  constructor(private api: ApiService, private router: Router, private route: ActivatedRoute ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      
      this.getItinerary(this.id)
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
  }

  getItinerary(id){
    this.api.getItinerary(id).subscribe((response)=>{
      this.itinerary = response;
      console.log(this.itinerary)
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
 
editAccomodation(data: any, id){
  window.sessionStorage.setItem("accomodation", JSON.stringify(data));
  window.sessionStorage.setItem("passitinerary", JSON.stringify(this.id));
  this.router.navigate(['/dashboard/accomodation', id]);

}
editDestination(data: any, id){
  window.sessionStorage.setItem("destination", JSON.stringify(data));
  window.sessionStorage.setItem("passitinerary", JSON.stringify(this.id));
  this.router.navigate(['/dashboard/destination', id]);

}
addDestination(id){
  this.router.navigate(['/dashboard/destination', id]);
  window.sessionStorage.setItem("passitinerary", JSON.stringify(this.id));
}
addAccomodation(id){
  this.router.navigate(['/dashboard/accomodation', id]);
  window.sessionStorage.setItem("passitinerary", JSON.stringify(this.id));
}
addDay(id){
  this.router.navigate(['/dashboard/day', id]);
}
addInclude(id){
  this.router.navigate(['/dashboard/includes', id]);
  window.sessionStorage.setItem("passitinerary", JSON.stringify(this.id));
}
addExclude(id){
  this.router.navigate(['/dashboard/excludes', id]);
  window.sessionStorage.setItem("passitinerary", JSON.stringify(this.id));
}

delete(type, id){
  this.api.delete(type,id).subscribe((response)=>{
  
    console.log(response)
    this.ngOnInit()
  },error =>{
    this.api.setLoadingStatus(false);
  })
}
preview(id){
  this.router.navigate(['/dashboard/preview', id]);
}
editDay(day){
  window.sessionStorage.setItem("passday", JSON.stringify(day));
  this.router.navigate(['/dashboard/day', this.id]);
}

}
