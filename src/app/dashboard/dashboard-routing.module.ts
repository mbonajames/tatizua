import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccomodationComponent } from './accomodation/accomodation.component';
import { AddItineraryComponent } from './add-itinerary/add-itinerary.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DayComponent } from './day/day.component';
import { DestinationComponent } from './destination/destination.component';
import { EditItineraryComponent } from './edit-itinerary/edit-itinerary.component';
import { ExcludesComponent } from './excludes/excludes.component';
import { IncludesComponent } from './includes/includes.component';
import { ItinerariesComponent } from './itineraries/itineraries.component';
import { PreviewComponent } from './preview/preview.component';
import { ProfileComponent } from './profile/profile.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { ReportsComponent} from './reports/reports.component';
import {AddEditContactComponent} from './add-edit-contact/add-edit-contact.component';
import {ServiceComponent} from './service/service.component';


const routes: Routes = [
  {path: 'dashboard', component: SidenavComponent, children: [
    {path: '', component:ReportsComponent},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'reports', component:ReportsComponent},
    {path: 'add-itinerary', component: AddItineraryComponent},
    {path: 'itineraries', component: ItinerariesComponent},
    {path: 'profile', component: ProfileComponent},
    {path: 'includes/:id', component: IncludesComponent},
    {path: 'excludes/:id', component: ExcludesComponent},
    {path: 'day/:id', component: DayComponent}, 
    {path: 'accomodation/:id', component: AccomodationComponent},
    {path: 'destination/:id', component: DestinationComponent},
    {path: 'preview/:id', component: PreviewComponent},
    {path: 'edit-itinerary/:id', component: EditItineraryComponent},
    {path:'add-contact',component:AddEditContactComponent},
    {path: 'edit-contact/:id', component:AddEditContactComponent},
    {path:'service',component:ServiceComponent},

    
    // {path: 'pending-added-services', component: PendingServicesComponent},
    // {path: 'pending-deleted-services', component: PendingDeletedServicesComponent},
    // {path: 'feedback', component: FeedbackComponent}

    
  ]},
  //  {path: 'dashboard', component: DashboardComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
