import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-add-itinerary',
  templateUrl: './add-itinerary.component.html',
  styleUrls: ['./add-itinerary.component.scss'],
})
export class AddItineraryComponent implements OnInit {
  itineraryForm: FormGroup;
  profile: any;
  countries: any;
  contacts: any;
  isLoading: boolean;
  error_msg: any;

  constructor(private fb: FormBuilder, private api: ApiService,  private router: Router,) {}

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const company_id = this.profile.id
    this.getCountries()
    this.getContact(company_id)

    this.itineraryForm = this.fb.group({
      company: [company_id, [Validators.required]],
      title: ['', [Validators.required]],
      client: ['', [Validators.required]],
      email: ['', [Validators.required]],
      people: ['', [Validators.required]],
      contact: ['', [Validators.required]],
      markup: ['', [Validators.required]],
      country: ['', [Validators.required]],
    });
  }
 

  addItinerary(data){
   console.log(data);
    let response = this.api.addItinerary(data);
    response.subscribe((response: any)=>{
     
      if (response) {
        this.api.setLoadingStatus(false);
        window.localStorage.setItem("itinerary", JSON.stringify(response.id));
          this.router.navigate(['/dashboard/includes',response.id]);
        }
        else {
         
      
        }
        
      
    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Payment required to use the system"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong Username or Password"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })

  }
  getCountries(){
    this.api.getCountries().subscribe((response)=>{
      this.countries = response;
      console.log(this.countries)
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })
  }

  getContact(id){
    this.api.getContact(id).subscribe((response)=>{
      this.contacts = response;
      console.log(this.contacts)
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
}
