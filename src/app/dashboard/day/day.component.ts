import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss']
})
export class DayComponent implements OnInit {

  dayForm: FormGroup;
  
  id: any;
  isLoading: boolean;
  error_msg: any;
  day: any;
  passday: any;
  constructor(private fb: FormBuilder, private api: ApiService, private router: Router, private route: ActivatedRoute ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      this.passday= JSON.parse(window.sessionStorage.getItem("passday"));
      
      
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });

    this.dayForm = this.fb.group({
      itinerary: [this.id, [Validators.required]],
      number: [this.passday ? this.passday.number: '', [Validators.required]],
      title: [this.passday ? this.passday.title:'', [Validators.required]],
      summary: [this.passday ? this.passday.summary:'', [Validators.required]],
      date: [this.passday ? this.passday.date:'', [Validators.required]],
    });
    this.api.setLoadingStatus(false);
    
  }
 
  addDay(data) {
    let response = this.api.addDay(data);
    response.subscribe((response: any)=>{
     
      if (response) {
        this.api.setLoadingStatus(false);
        this.day = response
        window.localStorage.setItem("itinerary", JSON.stringify(this.id))
        this.router.navigate(['/dashboard/accomodation', this.day.id]);
        }
        else {
          this.api.setLoadingStatus(false);
      
        }
        
      
    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Payment required to use the system"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong Username or Password"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })

  }
  editDay(data) {
    let response = this.api.editDay(this.passday.id,data);
    response.subscribe((response: any)=>{
      this.api.setLoadingStatus(false);
      this.router.navigate(['/dashboard/edit-itinerary', this.id]);
      window.sessionStorage.removeItem("passday");
      
    }, error =>{
     
       if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong inputs"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })

  }
cancel(){
  window.sessionStorage.removeItem("passday");
  this.router.navigate(['/dashboard/edit-itinerary', this.id]);
 
}
}
