import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-excludes',
  templateUrl: './excludes.component.html',
  styleUrls: ['./excludes.component.scss']
})
export class ExcludesComponent implements OnInit {
  excludesForm: FormGroup;
  id: any;
  isLoading: boolean;
  error_msg: any;
  excludes: any;
  passed_itinerary: any;
  constructor(private fb: FormBuilder, private api: ApiService, private router: Router, private route: ActivatedRoute ) {}

  ngOnInit(): void {
    this.passed_itinerary= JSON.parse(window.sessionStorage.getItem("passitinerary"));
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
    this.getExcludes(this.id)
   
    this.excludesForm = this.fb.group({
      itinerary: [this.id, [Validators.required]],
      description: ['', [Validators.required]],
    });
    
  }

  addExclude(data) {
    let response = this.api.addExclude(data);
    response.subscribe((response: any)=>{
     
      if (response) {
        this.api.setLoadingStatus(false);
        this.ngOnInit();
        }
        else {
          this.api.setLoadingStatus(false);
      
        }
        
      
    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Payment required to use the system"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong Username or Password"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })

  }
  getExcludes(id){
    this.api.getExcludes(id).subscribe((response)=>{
      this.excludes = response;
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  next(){
    this.router.navigate(['/dashboard/day', this.id]);
  }
  newSave(){
    this.router.navigate(['/dashboard/edit-itinerary', this.passed_itinerary]);
    window.sessionStorage.removeItem("passitinerary");
  }
}

