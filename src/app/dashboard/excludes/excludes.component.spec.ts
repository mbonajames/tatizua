import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcludesComponent } from './excludes.component';

describe('ExcludesComponent', () => {
  let component: ExcludesComponent;
  let fixture: ComponentFixture<ExcludesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExcludesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcludesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
