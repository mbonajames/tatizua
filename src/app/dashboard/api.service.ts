import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public error: BehaviorSubject<string> = new BehaviorSubject<string>('');

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  TOKEN: String;

 //BASE_URL = 'http://localhost:8000/api/';
 BASE_URL = 'https://tuasoft-api.herokuapp.com/api/';
 baseApiUrl = "https://file.io"

  constructor(private http: HttpClient) {}

  // LOADING STATUS FOR INDICATOR
  setLoadingStatus(status: boolean) {
    this.isLoading.next(status);
  }

  addItinerary(data: any) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-itinerary';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  } 
  getCountries() {
    this.setLoadingStatus(true);

     const url = this.BASE_URL + 'country';
    
    const response = this.http.get(url, this.httpOptions);

    return response;
  }  

  getContact(id){
    this.setLoadingStatus(true);

     const url = this.BASE_URL + 'contact?id=' + id;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  } 
  getServices() {
    this.setLoadingStatus(true);
     const url = this.BASE_URL + 'services'

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
  getIncludes(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-includes?id=' + id
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
  getExcludes(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-excludes?id=' + id
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
  getDailyAccomodations(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-daily-accomodations?id=' + id
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
  getDailydestinations(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-daily-destinations?id=' + id
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
  getAccomodations() {
    this.setLoadingStatus(true);

     const url = this.BASE_URL + 'accomodations'
  
    const response = this.http.get(url, this.httpOptions);
    return response;
  }  

  getDestinations() {
    this.setLoadingStatus(true);

     const url = this.BASE_URL + 'destinations'
    
    const response = this.http.get(url, this.httpOptions);

    return response;
  } 

  addInclude(data: any) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-include';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  } 

  addExclude(data: any) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-exclude';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  }

  addDay(data: any) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-day';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  }

  addDailyAccomodation(data: any) {
    
    this.setLoadingStatus(true);

     const url = this.BASE_URL + 'add-daily-accomodation';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  }

  addDailyDestination(data: any) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-daily-destination';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  }
  getItinerary(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-itinerary?id=' + id;
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
  
  getCompany(id, contact) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-company?id=' + id +'&contact=' + contact;
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }

  getCountry(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-country?id=' + id;
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }

  getItineraries(id) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-itineraries?id=' + id;
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  }
  updateAccomodation(params:string, data) {
    const url = this.BASE_URL + 'update-daily-accomodation/' + params;
  
    const response = this.http.patch(url, data, this.httpOptions);

    return response;
  }

  updateDestination(params:string, data) {
    const url = this.BASE_URL + 'update-daily-destination/' + params;
  
    const response = this.http.patch(url, data, this.httpOptions);

    return response;
  }
   //upload image for profile page
  upload(file):Observable<any>{
    const formData = new FormData();  
    formData.append("file", file, file.name); 
    return this.http.post(this.baseApiUrl  , formData) 


  }
  delete(type: string, id) {
    this.setLoadingStatus(true);
  
  
    let url = this.BASE_URL + 'delete?id=' + id + '&type=' + type;
  
    const response = this.http.delete(url, this.httpOptions);
  
    
  
    return response;
  }

  //updating contact & adding contact person
  updateContact(params:string, data){
      const url = this.BASE_URL + 'update-contact/' + params;
    
      const response = this.http.patch(url, data, this.httpOptions);
  
      return response;

  }
  
  addContact(data: any) {
    this.setLoadingStatus(true);
     const url = this.BASE_URL + 'add-contact';

    const response = this.http.post(url, data, this.httpOptions);
    return response;

  } 


  addService(data: any) 
  {
    this.setLoadingStatus(true);
    const url = this.BASE_URL + 'add-service';
    const response = this.http.post(url, data, this.httpOptions);
    return response;
  } 

  updateService(params:string, data) {
    const url = this.BASE_URL + 'update-service/' + params;
    const response = this.http.patch(url, data, this.httpOptions);
    return response; 
  }


  getService(id) {
    this.setLoadingStatus(true);
     const url = this.BASE_URL + 'services?id=' + id;
    const response = this.http.get(url, this.httpOptions);
    return response;
  }


updateCosts(id:any,data){
  const url = this.BASE_URL + 'update-costs/' + id;
  
  const response = this.http.patch(url, data, this.httpOptions);

  return response;

}
editDay(id:any,data){
  const url = this.BASE_URL + 'update-day/' + id;
  
  const response = this.http.patch(url, data, this.httpOptions);

  return response;
}

getReport(data){
  this.setLoadingStatus(true);
  const url=this.BASE_URL +'get-report';
  const response = this.http.post(url, data,this.httpOptions);
  return response

  
}
updateCompany(params:string, data) {
    const url = this.BASE_URL + 'update-company/' + params;
    const response = this.http.patch(url, data, this.httpOptions);
    return response;

}
getContacts() {
  this.setLoadingStatus(true);

   const url = this.BASE_URL + 'contact'

  const response = this.http.get(url, this.httpOptions);
  return response;
}  


}
