import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ApiService } from '../api.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-add-edit-contact',
  templateUrl: './add-edit-contact.component.html',
  styleUrls: ['./add-edit-contact.component.scss']
})
export class AddEditContactComponent implements OnInit {
  addContactform: FormGroup;
  id: any;
  loading = false;
  submitted = false;
  company:any;
  isLoading: boolean;
  contacts:any;
  contactData:any;
  edit:boolean;
  message:any;
  isadded:boolean;
  delete_id:any;
  delete_contact:any;


  

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiService,private location:Location) { }

  ngOnInit(): void {
 
    const profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const company_id = profile.id
    this.getcontact(company_id);
    this.getAllContacts()
    this.api.isLoading.subscribe((status: boolean) => {
    this.isLoading = status;
    });

    this.addContactform = this.formBuilder.group({
      company: [company_id, [Validators.required]],
      name:[this.contactData?this.contactData.name:'', Validators.required],
      phone:[this.contactData?this.contactData.phone:'', Validators.required],
      email:[this.contactData?this.contactData.email:'', [Validators.required, Validators.email]],
    });
 
   }

getcontact(id){
  this.api.getContact(id).subscribe((response)=>{
    this.contacts = response;
  
  },error =>{
    this.api.setLoadingStatus(false);
  })
}




   addContact(data){
    let response = this.api.addContact(data);
    response.subscribe((response: any)=>{
      if (response) {
        this.api.setLoadingStatus(false);
        this.message='contact person is added successfully'
        window.localStorage.setItem("contact", JSON.stringify(response.id));
          this.router.navigate(['dashboard/add-contact',response.id]);
          this.ngOnInit()
        }

      
      
    })
  }
   
  
   openDelete(id,name){
    this.delete_id = id
    this.delete_contact = name
   }



   delete(type, id){
    this.api.delete(type,id).subscribe((response)=>{
    
      console.log(response)
      this.ngOnInit()
    },error =>{
      this.api.setLoadingStatus(false);
    })
  }

  getAllContacts(){

  }
  openEditor(contact){
    this.contactData = contact
    this.edit= true
    this.message = null
    this.ngOnInit()
    
   }

  editContact(){
    let response = this.api.updateContact(this.contactData.id,this.addContactform.value);
    response.subscribe((res:any)=>{
      this.api.setLoadingStatus(false);
        this.message = "contact is successfully updated";
        this.contactData = null
        this.edit= false
        this.ngOnInit()
    },  error =>{
      this.api.setLoadingStatus(false);
      if (error.status == 400) {
        this.message = "service is not updated, Wrong Inputs";
      }
      else if (error.status == 0) {
        this.message = "Not updated, No internet Available";
      }
      
    })

  }

  


  goBack(): void {
    this.location.back();
  }



}
