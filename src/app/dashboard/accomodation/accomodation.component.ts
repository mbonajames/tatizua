import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-accomodation',
  templateUrl: './accomodation.component.html',
  styleUrls: ['./accomodation.component.scss']
})
export class AccomodationComponent implements OnInit {

  accomodationsForm: FormGroup;
  id: any;
  isLoading: boolean;
  error_msg: any;
  dailyAccomodation: any;
  services: any;
  accomodations: any;
  dailyaccomodations: any;
  passed_accomodation: any;
  passed_itinerary: any;
  constructor(private fb: FormBuilder, private api: ApiService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.passed_accomodation= JSON.parse(window.sessionStorage.getItem("accomodation"));
    this.passed_itinerary= JSON.parse(window.sessionStorage.getItem("passitinerary"));
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      
      this.api.isLoading.subscribe((status: boolean) => {
        this.isLoading = status;
      });
      this.getServices()
      this.getAccomodations()
      this.getDailyAccomodations(this.id)
   
    this.accomodationsForm = this.fb.group({
      day: [this.id, [Validators.required]],
      service: [this.passed_accomodation ? this.passed_accomodation.service: '', [Validators.required]],
      name: [this.passed_accomodation ? this.passed_accomodation.name: '', [Validators.required]],
      board: [this.passed_accomodation ? this.passed_accomodation.board: '', [Validators.required]],
      room: [this.passed_accomodation ? this.passed_accomodation.room: '', [Validators.required]],
      checkin: [this.passed_accomodation ? this.passed_accomodation.checkin: '', [Validators.required]],
      checkout: [this.passed_accomodation ? this.passed_accomodation.checkout: '', [Validators.required]],
      nights: [this.passed_accomodation ? this.passed_accomodation.nights: '', [Validators.required]],
    });
    
  }

  addAccomodation(data) {
    let response = this.api.addDailyAccomodation(data);
    response.subscribe((response: any)=>{
     
      if (response) {
        this.api.setLoadingStatus(false);
        this.dailyAccomodation = response
        this.ngOnInit();
        
        }
        else {
          this.api.setLoadingStatus(false);
      
        }
        
      
    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Payment required to use the system"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong Username or Password"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })

  }

  getServices(){
    this.api.getServices().subscribe((response)=>{
      this.services = response;
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  getAccomodations(){
    this.api.getAccomodations().subscribe((response)=>{
      this.accomodations = response;
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  getDailyAccomodations(id){
    this.api.getDailyAccomodations(id).subscribe((response)=>{
      this.dailyaccomodations = response;
      this.api.setLoadingStatus(false);
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }

  next(id){
    this.router.navigate(['/dashboard/destination', id]);
  }
  save(id){
    this.api.updateAccomodation(id,this.accomodationsForm.value).subscribe((response)=>{
      this.api.setLoadingStatus(false);
      this.router.navigate(['/dashboard/edit-itinerary', this.passed_itinerary]);
      window.sessionStorage.removeItem("accomodation");
      window.sessionStorage.removeItem("passitinerary");
    },error =>{
      this.api.setLoadingStatus(false);
    })

  }
  newSave(){
    this.router.navigate(['/dashboard/edit-itinerary', this.passed_itinerary]);
    window.sessionStorage.removeItem("passitinerary");
  }
}
