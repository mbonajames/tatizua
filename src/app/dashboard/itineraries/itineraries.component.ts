import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-itineraries',
  templateUrl: './itineraries.component.html',
  styleUrls: ['./itineraries.component.scss']
})
export class ItinerariesComponent implements OnInit {
  profile: any;
  countries: any;
  itineraries: any;
  isLoading: boolean;
  error_msg: any;

  constructor(private api: ApiService,  private router: Router,) {}

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const company_id = this.profile.id
    this.getItineraries(company_id)
  }
  getItineraries(id){
    this.api.getItineraries(id).subscribe((response)=>{
      this.itineraries = response;
      console.log(this.itineraries)
     
    },error =>{
      this.api.setLoadingStatus(false);
    })
  }
  edit(id){
    this.router.navigate(['/dashboard/edit-itinerary',id]);
  }
  delete(type, id){
    this.api.delete(type,id).subscribe((response)=>{
    
      console.log(response)
      this.ngOnInit()
    },error =>{
      this.api.setLoadingStatus(false);
    })
  }
}
